export const ADDTODO = 'ADDTODO'
export const TOGGLETODO = 'TOGGLETODO'
export const DELETETODO = 'DELETETODO';
export const CLEARCOMPLETED = 'CLEARCOMPLETED'

export const addTodo = (inputText) => {
    return {
        type: ADDTODO,
        payload: { inputText }
    }
};

export const toggleTodo = (id) => {
    return {
        type: TOGGLETODO,
        payload: { id }
    }
};

export const deleteTodo = (id) => {
    return {
        type: DELETETODO,
        payload: { id }
    }
};

export const clearCompleted = () => {
    return {
        type: CLEARCOMPLETED,
    }
}
