import React, { useState } from "react";
import {
  Switch,
  Route,
  NavLink
} from 'react-router-dom'
import TodoList from './components/todoList/TodoList';

import { connect } from 'react-redux'
import { addTodo, clearCompleted } from "./Actions/TodosActions";

function App(props) {


  const [inputText, setInputText] = useState('');

  const addTodo = (event) => {
    // console.log(event.which)
    if (event.which === 13) {
      props.addTodo(inputText)
      setInputText('');
    }
  }

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>

        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => addTodo(event)}
          className="new-todo"
          value={inputText}
          placeholder="What needs to be done?"
          autoFocus
        />

      </header>
      <Switch>
        <Route exact path='/'>
          <TodoList
            todos={Object.values(props.todos)}
          />
        </Route>
        <Route path='/active'>
          <TodoList
            todos={Object.values(props.todos).filter(todo =>
              !todo.completed
            )}
          />
        </Route>
        <Route path='/completed'>
          <TodoList
            todos={Object.values(props.todos).filter(todo =>
              todo.completed
            )}
          />
        </Route>
      </Switch>
      <footer className="footer">
        <span className="todo-count">
          <strong>{Object.values(props.todos).filter(todo => !todo.completed
          ).length}</strong> item(s) left
        </span>
        <ul className="filters">
          <li>
            <NavLink exact to="/" activeClassName='selected'>All</NavLink>
          </li>
          <li>
            <NavLink exact to="/active" activeClassName='selected'>Active</NavLink>
          </li>
          <li>
            <NavLink exact to="/completed" activeClassName='selected'>Completed</NavLink>
          </li>
        </ul>
        <button
          className="clear-completed"
          onClick={() => props.clearCompleted()}>
          Clear completed
          </button>
      </footer>
    </section>
  );
}

const mapStateToProps = state => {
  return {
    todos: state.todos
  }
};

const mapDispatchToProps = dispatch => {
  return{
    addTodo: inputText => dispatch(addTodo(inputText)),
    clearCompleted: () => dispatch(clearCompleted())
  }
}

export default
  connect(
    mapStateToProps,
    mapDispatchToProps)
    (App);




// }
  // handleText = (e) => {
  //   setState({ value: e.target.value })
  // };

  // handleSubmit = (e) => {
  //   if (e.key === 'Enter') {
  //     handleTodo()
  //   }
  // };

  // handleTodo = () => {
  //   const newTodo = {
  //     "userId": 1,
  //     "id": Math.floor(Math.random() * 30),
  //     "title": state.value,
  //     "completed": false
  //   };
  //   const addedTodo = [...state.todos, newTodo];
  //   setState({
  //     todos: addedTodo,
  //     value: ''
  //   })
  // };

  // handleRemove = (todoId) => {
  //   const removedTodos = state.todos.filter(
  //     (todoItem) => todoItem.id !== todoId
  //   );
  //   setState({ todos: removedTodos });
  // };

  // handleCheck = (checkId) => {
  //   const checkedTodos = state.todos.map(
  //     (todoItem) => {
  //       if (todoItem.id === checkId) {
  //         todoItem.completed = !todoItem.completed;
  //       }
  //       return todoItem;
  //     });
  //   setState({ todos: checkedTodos });
  // }

  // handleClearComplete = () => {
  //   const completeTodos = state.todos.filter(
  //     (todoItem) => todoItem.completed !== true
  //   );
  //   setState({ todos: completeTodos })
  // }
